import React, { Component } from "react";

class LifecycleExample extends Component {
// 실행순서 1번째
    constructor(props) {
        //이 코드에서 super(부모)는 Component이다. 상속해준 사람 찾기 ~~
        //super가 제일 먼저 온다.
        super(props);
        // 말하고 있는 주체가 constructor임
        // 여기서의 this는 LifecycleExample 스코프 안을 의미한다.
        this.state = {
            count: 0,
        };
    }

    // 실행순서 3번째
    componentDidMount() {
        console.log('Component mounted');
    }


    //prevProps는 count / prevState는 0이다.
    componentDidUpdate(prevProps, prevState) {
        // 만약에 값의 count와 이전의 count가 같지않으면
        // preState.count는 어디서 왔을까? 라이프사이클의 스냅샷
        if (this.state.count !== prevState.count) {
            // 현재 가지고 있는 값을 콘솔로 찍어라
            console.log('Count update: ', this.state.count);
        }
    }

    componentWillUnmount() {
        console.log('Component will unmount');
    }


    handleIncrement = () => {
        // 만약 count: this.state.count + 1로 한다면 원웨이 바인딩이기때문에 에러가 터질 수 있다.
        this.setState((prevState) => ({ count: prevState.count + 1 }));
    };


    render() {
        return (
            // 실행순서 2번째
            // this의 주체는 render
            // handleIncrement는 메서드
            // 이 다음으로 3번째로 실행되는 것은 초기화 단계이다.
            <div>
                <p>Count: {this.state.count}</p>
                <button onClick={this.handleIncrement}>Increment</button>
            </div>
        )
    }
}

export default LifecycleExample;