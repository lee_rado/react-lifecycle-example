import './App.css';
import LifecycleExample from "./components/LifecycleExample";

function App() {
  return (
    <div className="App">
      <LifecycleExample />
    </div>
  );
}

export default App;
